/*
 * @author: Mariano Anaya - marianoanaya@gmail.com
 * @date: Mon 28 Jul, 2014
 * */
#include "skiplist.h"

#define SENTINEL INT_MIN

SkipNode*
search(SkipLevel* skip_list, int x){
    /* Search for the value x in the <skip_list>,
     * and return the pointer of it, or NULL if not found.
     */
    SkipLevel* level = skip_list;
    SkipNode* pointer = NULL;
    SkipNode* ptr = NULL;

    /* skip_list starts at node called _header_ */
    pointer = level->start;
    for(ptr = pointer; ptr->below != NULL; ){
        if(ptr->next != NULL && ((SkipNode*)ptr->next)->value > x){
            ptr = (SkipNode*)ptr->next;
        } else {
            ptr = (SkipNode*)ptr->below;
        };
    };  /* last level reached, do linear search */

    for( ;
        ptr != NULL && ((SkipNode*)ptr)->value > x;
        ptr =(SkipNode*)ptr->next);
    if(ptr != NULL && ptr->value == x)
        return ptr;
    return NULL;
};


SkipNode*
search_previous(SkipLevel* skip_list, int x){
    /* Search for the value x in the <skip_list>,
     * and return the pointer previous to it, indicating the location
     * where the next element should be inserted.
     */
    SkipNode* ptr = NULL;
    /* skip_list starts at node called _header_ */
    for(ptr = skip_list->start; ptr->below != NULL; ){
        if(ptr->next != NULL && ((SkipNode*)ptr->next)->value < x){
            ptr = (SkipNode*)ptr->next;
        } else {
            ptr = (SkipNode*)ptr->below;
        };
    };  /* last level reached, do linear search */

    for( ; ptr != NULL && ptr->next != NULL && ((SkipNode*)ptr->next)->value < x;
           ptr = (SkipNode*)ptr->next);
    return ptr;
};


int
flip_a_coin(){
    sleep(1);  /* FIXME */
    srand(time(NULL));
    return random() % 2;  /* 0 || 1 */
};


int
insert(SkipLevel* skip_list, int x){
    /* Inserts element <x> into <skip_list> and returns 0 if succeeded or
     * -1 in case of error.
     */
    SkipNode* location = NULL;
    SkipNode* new_element = malloc(sizeof(SkipNode));
    SkipNode* upper_node = NULL;
    SkipLevel* level = NULL;
    SkipLevel* new_level = NULL;
    SkipNode* new_level_start_node = NULL;
    SkipNode* ptr = NULL;

    if (new_element == NULL)
        return -1;

    location = search_previous(skip_list, x);
    /* first insert into the last level */
    new_element->value = x;
    new_element->next = location->next;
    location->next = (SkipNode*)new_element;
    /* now check for the rest of the lists */
    level = skip_list;
    while(flip_a_coin() == HEADS){
        /* */
        upper_node = malloc(sizeof(SkipNode));
        if(upper_node == NULL)
            return -1;
        upper_node->value = x;
        upper_node->below = (SkipNode*)new_element;
        for(ptr = level->start;
            ptr != NULL && ptr->next != NULL && ((SkipNode*)ptr->next)->value < x;
            ptr = (SkipNode*)ptr->next);
        upper_node->next = ptr->next;
        ptr->next = (SkipNode*)upper_node;
        /* add new level */
        new_level = malloc(sizeof(SkipLevel));
        if(new_level == NULL)
            return -1;
        new_level->next = (SkipLevel*)level;  /* FIXME */
        new_level_start_node = malloc(sizeof(SkipNode));
        if(new_level_start_node == NULL)
            return -1;
        new_level_start_node->value = SENTINEL;
        new_level_start_node->next = NULL;
        new_level->start = new_level_start_node;
        level = new_level;
    };
    return 0;
};


SkipLevel*
create_skip_list(){
    /* Returns a pointer to the newly-created skip list
     * */
    SkipLevel* level_0 = malloc(sizeof(SkipLevel));
    SkipLevel* level_1 = malloc(sizeof(SkipLevel));
    SkipNode* sentinel_0 = malloc(sizeof(SkipNode));
    SkipNode* sentinel_1 = malloc(sizeof(SkipNode));

    sentinel_0->next = NULL;
    sentinel_0->value = SENTINEL;
    sentinel_1->next = NULL;
    sentinel_1->value = SENTINEL;
    sentinel_1->below = (SkipNode*)sentinel_0;
    sentinel_0->below = NULL;

    level_0->next = NULL;
    level_0->start = (SkipNode*)sentinel_0;

    level_1->next = (SkipNode*)level_0;
    level_1->start = (SkipNode*)sentinel_1;
    return level_1;
};
