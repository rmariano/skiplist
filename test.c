#include "skiplist.h"
#include <stdio.h>


int
print_skip_list(SkipLevel* skip_list){
    int i = 0;
    SkipLevel* level = NULL;
    SkipNode* list = NULL;

    printf("\n");
    for(level=skip_list; level != NULL; level = (SkipLevel*)level->next){
        printf("Level %d |", i);
        i++;
        for(list=level->start; list != NULL; list=(SkipNode*)list->next){
            printf("%d,", list->value);
        };
        printf("\n");
    }
    return 0;
};


int
main(){
    SkipLevel* skip_list = create_skip_list();

/* FIXME
 *  for(; i < 10; i++, printf("%d", flip_a_coin()) ); */

    insert(skip_list, 2);
    insert(skip_list, 3);
    insert(skip_list, 4);
    insert(skip_list, 1);
    print_skip_list(skip_list);
    return 0;
};
