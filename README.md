Skip Lists example
==================

Skips lists are an interesting data structure. This gist shows a small implementation
in ``C`` that stores ``int`` values.

Running Time:
============
O(lg n)
