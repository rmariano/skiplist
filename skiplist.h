#define SKIPLIST_H 1

#include <stdlib.h>  /* malloc, srand, random */
#include <time.h>    /* time */
#include <limits.h>  /* INT_MIN */
#include <unistd.h>  /* sleep */

#define HEADS 1
#define TAILS 0


typedef struct SkipNode{
    int value;
    SkipNode* next;
    SkipNode* below;
} SkipNode;

typedef struct SkipLevel{
    SkipNode* start;
    SkipLevel* next;  /* level */
} SkipLevel;

SkipNode* search(SkipLevel* skip_list, int x);
SkipNode* search_previous(SkipLevel* skip_list, int x);
int flip_a_coin();
int insert(SkipLevel* skip_list, int x);
SkipLevel* create_skip_list();
